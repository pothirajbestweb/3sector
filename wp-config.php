<?php

/**

 * The base configuration for WordPress

 *

 * The wp-config.php creation script uses this file during the

 * installation. You don't have to use the web site, you can

 * copy this file to "wp-config.php" and fill in the values.

 *

 * This file contains the following configurations:

 *

 * * MySQL settings

 * * Secret keys

 * * Database table prefix

 * * ABSPATH

 *

 * @link https://codex.wordpress.org/Editing_wp-config.php

 *

 * @package WordPress

 */


// ** MySQL settings - You can get this info from your web host ** //

/** The name of the database for WordPress */

define( 'DB_NAME', "3sectornew_db" );


/** MySQL database username */

define( 'DB_USER', "root" );


/** MySQL database password */

define( 'DB_PASSWORD', "" );


/** MySQL hostname */

define( 'DB_HOST', "localhost" );


/** Database Charset to use in creating database tables. */

define( 'DB_CHARSET', 'utf8mb4' );


/** The Database Collate type. Don't change this if in doubt. */

define( 'DB_COLLATE', '' );


/**#@+

 * Authentication Unique Keys and Salts.

 *

 * Change these to different unique phrases!

 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}

 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.

 *

 * @since 2.6.0

 */

define( 'AUTH_KEY',         'YU5%w`H,g&,]oSb>{edO&s_`k&JJ[D,a41Rv#mXyoR#!z)rqr&3<MFK3J#*<N1o<' );

define( 'SECURE_AUTH_KEY',  'U#(ss9ZQ`ws7W6DI&+1m=UQA@m<&f.&7yuOM`J{gpPtUk-zL m,d66,s3*:HSPO7' );

define( 'LOGGED_IN_KEY',    'w&+!C=ReDuq6d!^q`HLLzIAatv*DOQTG nH]mv`5&u-3]51;S9Oe|4.VM@x]5QPx' );

define( 'NONCE_KEY',        '(P6O%+(+&fGBu,AHUHXnA nNMH[Pb&IE:oD?JA`Mro+cb4R]J+.%KAizaCt=)g)*' );

define( 'AUTH_SALT',        'C{G^BGsxCMrhHM]PYZY^<ioxt(nz.V[O4x`@)^(?#RrNIu `;?wt46~#BPvEd7x|' );

define( 'SECURE_AUTH_SALT', 'CZt=CAF5Z.xeBCfkYcbjwM3UPA<8O9#x|Dfp5YGGIevL#cPQjT?f<7WO1K^ ~:p#' );

define( 'LOGGED_IN_SALT',   'K?mn)wHVYt9Er:OUH/uYM3$Ec5X;9YRo_j<&jK(vtQQX+JN?D|Mm,1nA)Srq@!,1' );

define( 'NONCE_SALT',       '`ZG5*b+Xd?njh1r8>][6shy%=r|5cuh{uM{?AaOWUG._;5`#G!!uN]9>OERh` P3' );


/**#@-*/


/**

 * WordPress Database Table prefix.

 *

 * You can have multiple installations in one database if you give each

 * a unique prefix. Only numbers, letters, and underscores please!

 */

$table_prefix = 'wp_';


/**

 * For developers: WordPress debugging mode.

 *

 * Change this to true to enable the display of notices during development.

 * It is strongly recommended that plugin and theme developers use WP_DEBUG

 * in their development environments.

 *

 * For information on other constants that can be used for debugging,

 * visit the Codex.

 *

 * @link https://codex.wordpress.org/Debugging_in_WordPress

 */

define( 'WP_DEBUG', false );


/* That's all, stop editing! Happy publishing. */


/** Absolute path to the WordPress directory. */

if ( ! defined( 'ABSPATH' ) ) {

	define( 'ABSPATH', dirname( __FILE__ ) . '/' );

}


/** Sets up WordPress vars and included files. */

require_once( ABSPATH . 'wp-settings.php' );


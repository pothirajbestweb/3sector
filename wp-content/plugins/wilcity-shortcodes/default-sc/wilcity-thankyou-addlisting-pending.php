<?php
function wilcityThankyouAddListingPending($aArgs, $content = '')
{
    if (!isset($_REQUEST['category']) || !in_array($_REQUEST['category'], ['paidClaim', 'addlisting'])) {
        return '';
    }
    
    if (!isset($_REQUEST['postID'])) {
        return '';
    }
    
    if (get_post_status($aArgs['postID']) !== 'pending') {
        return '';
    }
    
    return apply_filters('wilcity/thankyou-content', $content, [
        'postID'      => $_REQUEST['postID'],
        'promotionID' => $_REQUEST['promotionID'],
        'category'    => $_REQUEST['category']
    ]);
}

add_shortcode('wilcity_thankyou_addlisting_pending', 'wilcityThankyouAddListingPending');

<?php
namespace WilokeListingTools\Controllers;

use WilokeListingTools\Controllers\Retrieve\NormalRetrieve;
use WilokeListingTools\Framework\Helpers\General;
use WilokeListingTools\Framework\Helpers\GetWilokeSubmission;
use WilokeListingTools\Framework\Helpers\SetSettings;
use WilokeListingTools\Framework\Helpers\WooCommerce;
use WilokeListingTools\Framework\Payment\CreatedPaymentHook;
use WilokeListingTools\Framework\Payment\PaymentGatewayStaticFactory;
use WilokeListingTools\Framework\Payment\PaymentMethodInterface;
use WilokeListingTools\Framework\Payment\ProceededPaymentHook;
use WilokeListingTools\Framework\Payment\Receipt;
use WilokeListingTools\Framework\Payment\Receipt\ReceiptStructureInterface;
use WilokeListingTools\Framework\Payment\WooCommerce\WooCommerceChangePlan;
use WilokeListingTools\Framework\Payment\WooCommerce\WooCommerceNonRecurringCreatedPaymentHook;
use WilokeListingTools\Framework\Routing\Controller;
use WilokeListingTools\Framework\Store\Session;
use WilokeListingTools\Frontend\User;
use WilokeListingTools\Models\PaymentMetaModel;
use WilokeListingTools\Models\PaymentModel;
use WilokeListingTools\Models\PlanRelationshipModel;

class WooCommerceController extends Controller
{
    public $planID;
    public $productID;
    public $orderID;
    public $isNonRecurringPayment;
    public $nextBillingDateGMT;
    public $aPaymentMeta;
    public $oReceipt;
    protected $aPaymentIDs;
    public $gateway = 'woocommerce';
    private $excludeFromShopKey = 'exclude_from_shop';
    public $listingID;
    public $billingType;
    
    public function __construct()
    {
        //		add_action('wp_ajax_wiloke_change_plan_via_woocommerce', array($this, 'changePlan'));
        
        add_action('wiloke-listing-tools/before-redirecting-to-cart', [$this, 'removeProductFromCart'], 10, 1);
        add_filter('woocommerce_add_to_cart_validation', [$this, 'cleanEverythingBeforeAddProductToCart'], 0);
        add_action('woocommerce_add_to_cart', [$this, 'removeAssociatePlanItems'], 0);
        //        add_action('wiloke-listing-tools/payment-via-woocommerce', [$this, 'preparePayment'], 10, 2);
        
        add_action('woocommerce_thankyou', [$this, 'updateCategoryToOrderMeta'], 10, 1);
        //		add_action( 'woocommerce_order_status_pending', array($this, 'updateCategoryToOrderMeta'), 10, 1 );
        
        add_filter('woocommerce_payment_complete_order_status', [$this, 'autoCompleteOrder'], 10, 2);
        add_action('woocommerce_single_product_summary', [$this, 'removeGalleryOfWooBookingOnTheSidebar'], 1);
        add_action('wilcity/before-close-header-tag', [$this, 'addQuickCart']);
        add_action('wiloke-listing-tools/payment-pending', [$this, 'maybeSaveOldOrderIDIfItIsChangePlanSession']);
        
        /*
         * Exclude Add Listing Production From Shop page
         *
         * @since 1.2.0
         */
        add_action('updated_postmeta', [$this, 'addedListingProductToExcludeFromShopPage'], 10, 4);
        add_action('woocommerce_product_query', [$this, 'modifyWooQueryToExcludeShopPage'], 10);
        
        add_filter('wilcity/woocommerce/content-single-product/before-single-product-summary',
            [$this, 'willNotShowUpBeforeSingleProductIfIsBookingWidget']);
        add_filter('wilcity/woocommerce/content-single-product/after-single-product-summary',
            [$this, 'willNotShowUpBeforeSingleProductIfIsBookingWidget']);
        add_filter('wilcity/woocommerce/content-single-product/after-single-product',
            [$this, 'willNotShowUpBeforeSingleProductIfIsBookingWidget']);
    }
    
    /**
     * @param $status
     *
     * @return bool
     */
    public function willNotShowUpBeforeSingleProductIfIsBookingWidget($status)
    {
        return General::$isBookingFormOnSidebar ? false : $status;
    }
    
    /**
     * @param $metaID
     * @param $postID
     * @param $metaKey
     * @param $metaVal
     */
    public function addedListingProductToExcludeFromShopPage($metaID, $postID, $metaKey, $metaVal)
    {
        if ($metaKey == 'wilcity_woocommerce_association') {
            SetSettings::setPostMeta($metaVal, $this->excludeFromShopKey, 'yes');
        }
    }
    
    /**
     * @param $query
     *
     * @return bool
     */
    public function modifyWooQueryToExcludeShopPage($query)
    {
        if (!is_shop() && !is_product_category() && !is_product_tag()) {
            return false;
        }
        
        $aMetaQueries   = $query->get('meta_query');
        $aMetaQueries   = empty($aMetaQueries) ? [] : $aMetaQueries;
        $aMetaQueries[] = [
            'relation' => 'OR',
            [
                'key'     => 'wilcity_exclude_from_shop',
                'compare' => 'NOT EXISTS'
            ],
            [
                'key'     => 'wilcity_exclude_from_shop',
                'value'   => 'yes',
                'compare' => '!='
            ]
        ];
        
        $query->set('meta_query', $aMetaQueries);
    }
    
    /*
     * If it's change plan session, We will save old order id to Payment Meta.
     * This step is very important, because We will upgrade Listings that belong to old order plan to new order plan
     */
    public function maybeSaveOldOrderIDIfItIsChangePlanSession($aInfo)
    {
        $oldOrderID = Session::getSession(wilokeListingToolsRepository()->get('payment:wooOldOrderID'), true);
        
        if (!empty($oldOrderID)) {
            PaymentMetaModel::set($aInfo['paymentID'], 'oldOrderID', $oldOrderID);
        } else {
            // If the old plan is Free Plan
            $oldPaymentID = Session::getSession(wilokeListingToolsRepository()->get('payment:oldPaymentID'), false);
            if (!empty($oldPaymentID)) {
                if (PaymentModel::getField('userID', $oldPaymentID) == User::getCurrentUserID()) {
                    PaymentMetaModel::set($aInfo['paymentID'], 'oldPaymentID', $oldPaymentID);
                }
            }
        }
    }
    
    /*
     * Change WooCommerce Subscription Plan
     *
     * @since 1.1.7.3
     */
    public function changePlan()
    {
        if (!isset($_POST['newPlanID']) || !isset($_POST['currentPlanID']) || !isset($_POST['paymentID']) ||
            !isset($_POST['postType'])
        ) {
            wp_send_json_error([
                'msg' => esc_html__('ERROR: The new plan, current plan, post type and payment ID are required',
                    'wiloke-listing-tools')
            ]);
        }
        
        $userID = get_current_user_id();
        $this->middleware(['isMyPaymentSession'], [
            'paymentID' => abs($_POST['paymentID']),
            'userID'    => $userID
        ]);
        
        $oWooCommerceChangePlan =
            new WooCommerceChangePlan($userID, $_POST['paymentID'], $_POST['newPlanID'], $_POST['currentPlanID'],
                $_POST['postType']);
        $aStatus                = $oWooCommerceChangePlan->execute();
        
        if ($aStatus['status'] == 'success') {
            wp_send_json_success($aStatus);
        } else {
            wp_send_json_error($aStatus);
        }
    }
    
    public function addQuickCart()
    {
        if (class_exists('woocommerce')) {
            // Is this the cart page?
            if (is_cart() || WC()->cart->get_cart_contents_count() == 0 || is_page_template
                ('dashboard/index.php')
            ) {
                return false;
            }
            ?>
            <div class="header_cartWrap__bOA2i active widget woocommerce widget_shopping_cart">
                <div class="header_cartBtn__1gAQU">
                    <span class="<?php echo esc_attr(apply_filters('wilcity/filter/class-prefix',
                        'wilcity-total-cart-item')); ?>"><?php echo esc_html(WC()->cart->get_cart_contents_count()); ?></span>
                    <div class="header_cartIcon__18VjH">
                        <i class="la la-shopping-cart"></i>
                    </div>
                    <div class="header_product__1q6pw product-cart-js">
                        <header class="header_cartHeader__2LxzS"><h4 class="header_cartTitle__l46ln"><i
                                        class="la la-shopping-cart"></i><?php echo esc_html__('Total Items',
                                    'wiloke-listing-tools'); ?> <span
                                        class="<?php echo esc_attr(apply_filters('wilcity/filter/class-prefix',
                                            'wilcity-total-cart-item')); ?>"><?php echo esc_html(WC()->cart->get_cart_contents_count()); ?></span>
                            </h4></header>

                        <div class="widget_shopping_cart_content">
                            <?php woocommerce_mini_cart(); ?>
                        </div>
                    </div>
                </div>
            </div>
            <?php
        }
    }
    
    public function removeGalleryOfWooBookingOnTheSidebar()
    {
        if (General::$isBookingFormOnSidebar) {
            remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_title', 5);
            remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_rating', 10);
            remove_action('woocommerce_template_single_price', 'woocommerce_template_single_price', 15);
        }
    }
    
    public function autoCompleteOrder($status, $orderID)
    {
        $order         = wc_get_order($orderID);
        $paymentMethod = get_post_meta($orderID, '_payment_method', true);
        // No updated status for orders delivered with Bank wire, Cash on delivery and Cheque payment methods.
        
        if (in_array($paymentMethod, ['bacs', 'cod', 'cheque'])) {
            return $status;
        } elseif ($order->has_status('processing')) {
            $paymentID = PaymentModel::getPaymentIDsByWooOrderID($orderID, true);
            if (!empty($paymentID)) {
                $status = 'completed';
            }
        }
        
        return $status;
    }
    
    public function cleanEverythingBeforeAddProductToCart($cart_item_data)
    {
        
        if (!isset($_GET['add-to-cart']) || empty($_GET['add-to-cart']) && !is_cart()) {
            return $cart_item_data;
        }
        
        global $woocommerce;
        
        $planID = PlanRelationshipModel::getPlanIDByProductID($_GET['add-to-cart']);
        if (empty($planID)) {
            return $cart_item_data;
        }
        
        $woocommerce->cart->empty_cart();
        
        return true;
    }
    
    public function removeAssociatePlanItems()
    {
        global $woocommerce;
        if ($woocommerce->cart->get_cart_contents_count() == 0) {
            return false;
        }
        
        $productID = Session::getSession(wilokeListingToolsRepository()->get('payment:associateProductID'));
        if (empty($productID)) {
            return false;
        }
        
        foreach ($woocommerce->cart->get_cart() as $cartItemKey => $aCardItem) {
            $planID = PlanRelationshipModel::getPlanIDByProductID($productID);
            if (empty($planID)) {
                continue;
            }
            
            if ($aCardItem['product_id'] != $productID) {
                $woocommerce->cart->remove_cart_item($cartItemKey);
            }
        }
    }
    
    public function removeProductFromCart($productIDs)
    {
        global $woocommerce;
        foreach ($woocommerce->cart->get_cart() as $cartItemKey => $aCardItem) {
            if (is_array($productIDs)) {
                if (in_array($aCardItem['product_id'], $productIDs)) {
                    $woocommerce->cart->remove_cart_item($cartItemKey);
                }
            } else {
                if ($aCardItem['product_id'] == $productIDs) {
                    $woocommerce->cart->remove_cart_item($cartItemKey);
                }
            }
        }
    }
    
    public function updateCategoryToOrderMeta($orderID)
    {
        $paymentID = PaymentModel::getPaymentIDsByWooOrderID($orderID, true);
        if (empty($paymentID)) {
            return false;
        }
        
        $category = Session::getPaymentCategory();
        
        if (!empty($category)) {
            wc_update_order_item_meta($orderID, '_wilcity_plan_category', $category);
        }
    }
}

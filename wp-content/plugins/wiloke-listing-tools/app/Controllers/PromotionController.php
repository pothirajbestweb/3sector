<?php

namespace WilokeListingTools\Controllers;

use Stripe\Util\Set;
use WilokeListingTools\Controllers\Retrieve\AjaxRetrieve;
use WilokeListingTools\Framework\Helpers\FileSystem;
use WilokeListingTools\Framework\Helpers\General;
use WilokeListingTools\Framework\Helpers\GetSettings;
use WilokeListingTools\Framework\Helpers\GetWilokeSubmission;
use WilokeListingTools\Framework\Helpers\SetSettings;
use WilokeListingTools\Framework\Helpers\Time;
use WilokeListingTools\Framework\Payment\PaymentGatewayStaticFactory;
use WilokeListingTools\Framework\Payment\PayPal\PayPalExecuteNonRecurringPayment;
use WilokeListingTools\Framework\Payment\PayPal\PayPalExecutePromotionPayment;
use WilokeListingTools\Framework\Payment\Receipt\ReceiptStaticFactory;
use WilokeListingTools\Framework\Payment\WooCommerce\WoocommerceNonRecurringPayment;
use WilokeListingTools\Framework\Routing\Controller;
use WilokeListingTools\Framework\Store\Session;
use WilokeListingTools\Frontend\User;
use WilokeListingTools\Models\PaymentMetaModel;
use WilokeListingTools\Models\PaymentModel;
use WilokeListingTools\Models\PromotionModel;

class PromotionController extends Controller
{
    protected $aPromotionPlans;
    protected $aWoocommercePlans;
    protected $expirationHookName = 'expiration_promotion';
    private $belongsToPromotionKey = 'belongs_to_promotion';
    private $category = 'promotion';
    protected $logFileName = 'promotion-success.log';
    private $oReceipt;
    private $isNonRecurringPayment = true;
    private $gateway;
    private $aSelectedPlans;
    private $aSelectedPlanKeys;
    
    public function __construct()
    {
        add_action('wp_ajax_wilcity_fetch_promotion_plans', [$this, 'fetchPromotions']);
        add_action('wp_ajax_wilcity_get_payment_gateways', [$this, 'getPaymentGateways']);
        add_action('wp_ajax_wilcity_boost_listing', [$this, 'boostListing']);
        
        add_action('updated_post_meta', [$this, 'updatedListingPromotionMeta'], 10, 4);
        add_action('added_post_meta', [$this, 'updatedListingPromotionMeta'], 10, 4);
        add_action('post_updated', [$this, 'updatePromotion'], 10, 3);
        add_action('before_delete_post', [$this, 'deletePromotion'], 100);
        
        $aPromotionPlans = GetSettings::getOptions('promotion_plans');
        if (!empty($aPromotionPlans)) {
            foreach ($aPromotionPlans as $aPlanSetting) {
                add_action($this->generateScheduleKey($aPlanSetting['position']), [
                    $this,
                    'deletePromotionValue'
                ], 10, 2);
            }
        }
        
        add_action('wilcity/single-listing/sidebar-promotion', [$this, 'printOnSingleListing'], 10, 2);
        add_action('wp_ajax_wilcity_fetch_listing_promotions', [$this, 'fetchPromotionDetails']);
        add_action('wilcity/wiloke-listing-tools/inserted-payment', [$this, 'createPromotion']);
        
        $aBillingTypes = wilokeListingToolsRepository()->get('payment:billingTypes', false);
        foreach ($aBillingTypes as $billingType) {
            add_action(
                'wilcity/wiloke-listing-tools/'.$billingType.'/payment-completed',
                [$this, 'updatePromotionAfterPaymentCompleted'],
                15
            );
            
            add_action(
                'wilcity/wiloke-listing-tools/'.$billingType.'/payment-refunded',
                [$this, 'removePromotionAfterPaymentRefunded'],
                15
            );
        }
        
        add_action('init', [$this, 'paymentExecution'], 1);
        add_action('woocommerce_checkout_order_processed', [$this, 'purchasePromotionPlansThroughWooCommerce'], 5, 1);
    }
    
    public function paymentExecution()
    {
        if (
            Session::getSession('waiting_for_paypal_execution') !== 'yes'
            || !isset($_GET['category']) || !in_array($_GET['category'], ['promotion'])
            || !isset($_GET['category']) || empty($_GET['category'])
            || !isset($_GET['token']) || empty($_GET['token'])
        ) {
            return false;
        }
        
        $oPayPalMethod = new PayPalExecutePromotionPayment(new PayPalExecuteNonRecurringPayment());
        
        if (!$oPayPalMethod) {
            return false;
        }
        
        if ($oPayPalMethod->verify()) {
            $aResponse = $oPayPalMethod->execute();
            if ($aResponse['status'] == 'error') {
                Session::setSession('payment_error', $aResponse['msg']);
                FileSystem::logError($aResponse['msg'], __CLASS__, __METHOD__);
            }
        }
    }
    
    public function fetchPromotionDetails()
    {
        if (!isset($_POST['postID']) || empty($_POST['postID'])) {
            wp_send_json_error([
                'msg' => esc_html__('The post id is required.', 'wiloke-listing-tools'),
            ]);
        }
        
        $aRawPromotions = PromotionModel::getListingPromotions($_POST['postID']);
        if (empty($aRawPromotions)) {
            wp_send_json_error([
                'msg' => esc_html__('There are no promotions.', 'wiloke-listing-tools'),
            ]);
        }
        
        $aPromotions        = [];
        $aRawPromotionPlans = GetSettings::getPromotionPlans();
        
        $aPromotionPlans = [];
        foreach ($aRawPromotionPlans as $promotionKey => $aPlan) {
            $aPromotionPlans[$promotionKey] = $aPlan;
        }
        
        foreach ($aRawPromotions as $aPromotion) {
            $position      = str_replace('wilcity_promote_', '', $aPromotion['meta_key']);
            $aPromotions[] = [
                'name'     => $aPromotionPlans[$position]['name'],
                'position' => $position,
                'preview'  => $aPromotionPlans[$position]['preview'],
                'expiryOn' => date_i18n(get_option('date_format'), $aPromotion['meta_value'])
            ];
        }
        
        wp_send_json_success($aPromotions);
    }
    
    public function printOnSingleListing($post, $aSidebarSetting)
    {
        $aSidebarSetting = wp_parse_args($aSidebarSetting, [
            'name'        => '',
            'conditional' => '',
            'promotionID' => '',
            'style'       => 'slider'
        ]);
        
        $belongsTo = GetSettings::getPostMeta($post->ID, 'belongs_to');
        
        if (!empty($belongsTo) && !GetSettings::isPlanAvailableInListing($post->ID, 'toggle_promotion')) {
            return '';
        }
        
        $aPromotionSettings = GetSettings::getPromotionSetting($aSidebarSetting['promotionID']);
        
        if (!is_array($aPromotionSettings)) {
            return $aPromotionSettings;
        }
        
        $aSidebarSetting = array_merge($aSidebarSetting, $aPromotionSettings);
        
        $aSidebarSetting['orderby']                       = 'rand';
        $aSidebarSetting['order']                         = 'DESC';
        $aSidebarSetting['postType']                      = General::getPostTypeKeys(false, false);
        $aSidebarSetting['aAdditionalArgs']['meta_query'] = [
            [
                'key'     => GetSettings::generateListingPromotionMetaKey($aSidebarSetting, true),
                'compare' => 'EXISTS',
            ]
        ];
        
        $aAtts = [
            'atts' => $aSidebarSetting
        ];
        
        echo wilcitySidebarRelatedListings($aAtts);
    }
    
    public function deletePromotion($postID)
    {
        if (get_post_type($postID) != 'promotion') {
            return false;
        }
        
        $listingID = GetSettings::getPostMeta($postID, 'listing_id');
        if (empty($listingID)) {
            return false;
        }
        SetSettings::deletePostMeta($listingID, $this->belongsToPromotionKey);
    }
    
    private function focusUpdatePromotionStatus($postID, $status)
    {
        global $wpdb;
        $wpdb->update(
            $wpdb->posts,
            [
                'post_status' => $status
            ],
            [
                'ID' => $postID
            ],
            [
                '%s'
            ],
            [
                '%s'
            ]
        );
    }
    
    public function updateChangedPaymentStatus($aData)
    {
        if ($aData['newStatus'] !== 'pending') {
            $aBoostPostData = PaymentMetaModel::get($aData['paymentID'], 'boost_post_data');
            SetSettings::deletePostMeta($aBoostPostData['postID'], 'promotion_wait_for_bank_transfer');
        } else {
            $aBoostPostData = PaymentMetaModel::get($aData['paymentID'], 'boost_post_data');
            SetSettings::setPostMeta($aBoostPostData['postID'], 'promotion_wait_for_bank_transfer',
                $aData['paymentID']);
        }
    }
    
    public function deleteWaitForBankTransferStatus($aData)
    {
        if (PaymentMetaModel::get($aData['paymentID'], 'packageType') !== 'promotion') {
            return false;
        }
    }
    
    public function updatePromotionPageStatusToPublish($aData)
    {
        $promotionPageID = PaymentMetaModel::get($aData['paymentID'], 'promotion_page_id');
        if (empty($promotionPageID)) {
            return false;
        }
        
        wp_update_post([
            'ID'          => $promotionPageID,
            'post_status' => 'draft'
        ]);
        
        wp_update_post([
            'ID'          => $promotionPageID,
            'post_status' => 'publish'
        ]);
    }
    
    public function deletePromotionValue($listingID, $position)
    {
        SetSettings::deletePostMeta($listingID, 'promote_'.$position);
        
        if (strpos($position, 'top_of_search') !== false) {
            $this->updateMenuOrder($listingID, $position, false);
        }
        
        $promotionID = GetSettings::getPostMeta($listingID, $this->belongsToPromotionKey);
        if (empty($promotionID)) {
            return false;
        }
        $aRawPromotionPlans = GetSettings::getOptions('promotion_plans');
        
        $isExpiredAll = true;
        $now          = current_time('timestamp');
        
        foreach ($aRawPromotionPlans as $aPlanSetting) {
            $val = GetSettings::getPostMeta($promotionID, 'promote_'.$aPlanSetting['position']);
            if (!empty($val)) {
                $val = abs($val);
                if ($val > $now) {
                    $isExpiredAll = false;
                }
            }
        }
        
        if ($isExpiredAll) {
            $this->focusUpdatePromotionStatus($promotionID, 'draft');
        }
    }
    
    private function deleteAllPlansOfListing($listingID)
    {
        $aRawPromotionPlans = GetSettings::getOptions('promotion_plans');
        foreach ($aRawPromotionPlans as $aPlanSetting) {
            if (strpos($aPlanSetting['position'], 'top_of_search') !== false) {
                $this->updateMenuOrder($listingID, $aPlanSetting['position'], false);
            }
            
            $aPromotionPlans = $this->getPromotionPlans();
            $aPlanKeys       = array_keys($aPromotionPlans);
            
            foreach ($aPlanKeys as $position) {
                $this->clearExpirationPromotion($position, $listingID);
                if (strpos($position, 'top_of_search') !== false) {
                    $promotionExists = GetSettings::getPostMeta($listingID, 'promote_'.$position);
                    if (!empty($promotionExists)) {
                        $this->updateMenuOrder($listingID, $position, false);
                    }
                }
                SetSettings::deletePostMeta($listingID, 'promote_'.$position);
            }
        }
        
        SetSettings::deletePostMeta($listingID, $this->belongsToPromotionKey);
    }
    
    /*
     * Generate Key where stores promotion duration to Listing ID
     *
     * @since 1.2.0
     */
    private function generateListingPromoteKey($aPromotion)
    {
        return 'promote_'.GetSettings::generateSavingPromotionDurationKey($aPromotion);
    }
    
    private function generateScheduleKey($position)
    {
        return 'trigger_promote_'.$position.'_expired';
    }
    
    protected function getPromotionPlans()
    {
        $this->aPromotionPlans = GetSettings::getPromotionPlans();
        
        return $this->aPromotionPlans;
    }
    
    protected function getPromotionPlanKeys()
    {
        return is_array($this->getPromotionPlans()) ? array_keys($this->aPromotionPlans) : [];
    }
    
    public function getPromotionField($field)
    {
        $this->getPromotionPlans();
        
        return isset($this->aPromotionPlans[$field]) ? $this->aPromotionPlans[$field] : false;
    }
    
    /*
     * Updating Listing Order
     *
     * @since 1.0
     */
    private function updateMenuOrder($listingID, $promotionKey, $isPlus = true)
    {
        $promotionKey = str_replace('wilcity_promote_', '', $promotionKey);
        
        $aTopOfSearchSettings = $this->getPromotionField($promotionKey);
        if ($aTopOfSearchSettings) {
            $menuOrder = get_post_field('menu_order', $listingID);
            if ($isPlus) {
                $menuOrder = abs($menuOrder) + abs($aTopOfSearchSettings['menu_order']);
            } else {
                $menuOrder = abs($menuOrder) - abs($aTopOfSearchSettings['menu_order']);
            }
            
            global $wpdb;
            $wpdb->update(
                $wpdb->posts,
                [
                    'menu_order' => $menuOrder
                ],
                [
                    'ID' => $listingID
                ],
                [
                    '%d'
                ],
                [
                    '%d'
                ]
            );
        }
    }
    
    /**
     * Set a expiry promotion cron job
     *
     * $position This var contains promotion ID already
     * @since 1.2.0
     * @return null
     */
    public function updatedListingPromotionMeta($metaID, $objectID, $metaKey, $metaValue)
    {
        if ((get_post_type($objectID) !== 'promotion')) {
            return false;
        }
        
        if (strpos($metaKey, 'wilcity_promote_') !== false) {
            $listingID = GetSettings::getPostMeta($objectID, 'listing_id');
            
            if (empty($listingID)) {
                return false;
            }
            
            $position = str_replace('wilcity_promote_', '', $metaKey);
            $this->clearExpirationPromotion($position, $listingID);
            
            if (is_numeric($metaValue) && !empty($metaValue)) {
                wp_clear_scheduled_hook($this->generateScheduleKey($position), [$listingID, $position]);
                wp_schedule_single_event($metaValue, $this->generateScheduleKey($position), [
                    $listingID,
                    $position
                ]);
                
                update_post_meta($listingID, $metaKey, $metaValue);
                
                if (strpos($metaKey, 'top_of_search') !== false) {
                    $this->updateMenuOrder($listingID, $metaKey, true);
                }
            }
        } else if ($metaKey == 'wilcity_listing_id') {
            SetSettings::setPostMeta($metaValue, $this->belongsToPromotionKey, $objectID);
            FileSystem::logPayment($this->logFileName, $metaValue.' '.$objectID);
        }
    }
    
    public function updatePromotion($postID, $oPostAfter, $oPostBefore)
    {
        if ($oPostAfter->post_type !== 'promotion') {
            return false;
        }
        
        // Processing this update under Promotion
        if (isset($_GET['action']) && $_GET['action'] == 'edit') {
            $listingID = abs($_POST['wilcity_listing_id']);
            $isAdmin   = false;
        } else {
            $listingID = GetSettings::getPostMeta($postID, 'listing_id');
            $isAdmin   = true;
        }
        
        if (empty($listingID)) {
            return false;
        }
        
        $aPromotionPlans = $this->getPromotionPlans();
        if (empty($aPromotionPlans)) {
            return false;
        }
        
        $isFocusReSetupPromotion = false;
        
        if ($oPostAfter->post_status == 'publish') {
            $currentListingID = GetSettings::getPostMeta($postID, 'listing_id');
            
            if (!empty($currentListingID) && $listingID != $currentListingID) {
                $this->deleteAllPlansOfListing($currentListingID);
                $isFocusReSetupPromotion = true;
            }
            
            SetSettings::setPostMeta($listingID, $this->belongsToPromotionKey, $postID);
            
            if (($oPostAfter->post_status != $oPostBefore->post_status) || $isFocusReSetupPromotion) {
                $aPromotionKeys = $this->getPromotionPlanKeys();
                if ($aPromotionKeys) {
                    foreach ($aPromotionKeys as $position) {
                        $metakey = 'promote_'.$position;
                        if ($isAdmin) {
                            $promotionKey = 'wilcity_promote_'.$position;
                            if (isset($_POST[$promotionKey])) {
                                SetSettings::setPostMeta($postID, $metakey, $_POST[$promotionKey]);
                            }
                        } else {
                            $val = GetSettings::getPostMeta($postID, $metakey);
                            if (!empty($val)) {
                                SetSettings::setPostMeta($postID, $metakey, $val);
                            }
                        }
                    }
                }
            }
            
            do_action('wiloke/promotion/approved', $listingID);
        } else {
            $this->deleteAllPlansOfListing($listingID);
        }
    }
    
    protected function getWooCommercePlanSettings($productID)
    {
        $aPromotionPlans = $this->getPromotionPlans();
        foreach ($aPromotionPlans as $aPromotion) {
            if ($aPromotion['productAssociation'] == $productID) {
                return $aPromotion;
            }
        }
    }
    
    protected function getWooCommercePlans()
    {
        if (!empty($this->aWoocommercePlans)) {
            return $this->aWoocommercePlans;
        }
        
        $aPromotionPlans = $this->getPromotionPlans();
        foreach ($aPromotionPlans as $aPromotion) {
            $this->aWoocommercePlans[] = $aPromotion['productAssociation'];
        }
        
        return $this->aWoocommercePlans;
    }
    
    public function cancelPostPromotion($aInfo)
    {
        $aBoostPostData = PaymentMetaModel::get($aInfo['paymentID'], 'boost_post_data');
        if (empty($aBoostPostData)) {
            return true;
        }
        $this->decreasePostPromotion($aBoostPostData);
    }
    
    protected function clearExpirationPromotion($position, $postID)
    {
        wp_clear_scheduled_hook($this->generateScheduleKey($position), [$postID, $position]);
        wp_clear_scheduled_hook($this->generateScheduleKey($position), ["$postID", "$position"]);
    }
    
    public function decreasePostPromotion($aBoostPostData)
    {
        foreach ($aBoostPostData['plans'] as $aInfo) {
            $this->clearExpirationPromotion($aInfo['position'], $aBoostPostData['postID']);
            SetSettings::deletePostMeta($aBoostPostData['postID'], $aInfo['position']);
        }
    }
    
    public function updatePostPromotion($aBoostPostData)
    {
        $promotionID = GetSettings::getPostMeta($aBoostPostData['postID'], $this->belongsToPromotionKey);
        
        if (empty($promotionID)) {
            $promotionID = wp_insert_post([
                'post_title'  => 'Promote '.get_the_title($aBoostPostData['postID']),
                'post_type'   => 'promotion',
                'post_status' => 'draft',
                'post_author' => $aBoostPostData['userID']
            ]);
        }
        
        $aPaymentMetaInfo = PaymentMetaModel::getPaymentInfo($aBoostPostData['postID']);
        
        SetSettings::setPostMeta($promotionID, 'listing_id', $aBoostPostData['postID']);
        foreach ($aPaymentMetaInfo['aSelectedPlans'] as $aInfo) {
            SetSettings::setPostMeta($promotionID, $this->generateListingPromoteKey($aInfo),
                strtotime('+ '.$aInfo['duration'].' days'));
        }
        
        wp_update_post([
            'ID'          => $promotionID,
            'post_status' => 'publish'
        ]);
        
        return $promotionID;
    }
    
    private function isPromotionProduct($productID)
    {
        $aPromotionPlans = $this->getPromotionPlans();
        
        // If $planID is not empty, which means it's Add Listing Plan Submission
        foreach ($aPromotionPlans as $aPromotionPlan) {
            if ($aPromotionPlan['productAssociation'] == $productID) {
                $this->aSelectedPlans[]    = $aPromotionPlan;
                $this->aSelectedPlanKeys[] = GetSettings::generateSavingPromotionDurationKey($aPromotionPlan);
                
                return true;
            }
        }
        
        return false;
    }
    
    public function purchasePromotionPlansThroughWooCommerce($orderID)
    {
        $oOrder = new \WC_Order($orderID);
        $aItems = $oOrder->get_items();
        
        $aPromotionProductIDs = [];
        
        foreach ($aItems as $aItem) {
            $productID = $aItem['product_id'];
            if ($this->isPromotionProduct($productID)) {
                $aPromotionProductIDs[] = $productID;
            }
        }
        
        $oRetrieve = new RetrieveController(new AjaxRetrieve());
        
        if (!empty($aPromotionProductIDs)) {
            $this->gateway = 'woocommerce';
            $postID        = Session::getPaymentObjectID();
            
            $aMiddleware = ['isGatewaySupported', 'isPublishedPost'];
            $aStatus     = $this->middleware($aMiddleware, [
                'postID'    => $postID,
                'gateway'   => $this->gateway,
                'isBoolean' => true
            ]);
            
            if ($aStatus['status'] == 'error') {
                return $oRetrieve->error($aStatus);
            }
            
            $this->oReceipt = ReceiptStaticFactory::get($this->category, [
                'userID'            => User::getCurrentUserID(),
                'couponCode'        => isset($aData['couponCode']) ? $aData['couponCode'] : '',
                'productID'         => $aPromotionProductIDs,
                'orderID'           => $orderID,
                'aRequested'        => $_REQUEST,
                'aSelectedPlanKeys' => $this->aSelectedPlanKeys,
                'planName'          => sprintf(
                    esc_html__('Promote %s', 'wiloke-listing-tools'),
                    get_the_title($postID)
                ),
                'gateway'           => $this->gateway,
                'aProductIDs'       => $aPromotionProductIDs
            ]);
            $this->oReceipt->setupPlan();
            
            $aResponse = $this->createSession();
            
            $oRetrieve = new RetrieveController(new AjaxRetrieve());
            if ($aResponse['status'] != 'success') {
                return $oRetrieve->error($aResponse);
            }
        }
    }
    
    public function createPromotion($aInfo)
    {
        if (!isset($aInfo['category']) || $aInfo['category'] != $this->category) {
            return false;
        }
        
        $promotionID = GetSettings::getPostMeta($aInfo['postID'], $this->belongsToPromotionKey);
        
        if (empty($promotionID)) {
            $promotionID = wp_insert_post([
                'post_title'  => 'Promote '.get_the_title($aInfo['postID']),
                'post_type'   => 'promotion',
                'post_status' => 'draft',
                'post_author' => $aInfo['userID']
            ]);
        } else {
            wp_update_post(
                [
                    'ID'          => $promotionID,
                    'post_status' => 'draft'
                ]
            );
        }
        
        SetSettings::setPostMeta($promotionID, 'listing_id', $aInfo['postID']);
        foreach ($aInfo['aSelectedPlans'] as $aPlan) {
            SetSettings::setPostMeta(
                $promotionID,
                $this->generateListingPromoteKey($aPlan),
                strtotime('+ '.$aPlan['duration'].' days')
            );
        }
        
        PaymentMetaModel::setPromotionID($aInfo['paymentID'], $promotionID);
        Session::setSession('promotionID', $promotionID);
    }
    
    public function updatePromotionAfterPaymentCompleted($aInfo)
    {
        if (!isset($aInfo['category']) || $aInfo['category'] != $this->category) {
            return false;
        }
        
        $promotionID = PaymentMetaModel::getPromotionID($aInfo['paymentID']);
        
        if (empty($promotionID)) {
            FileSystem::logError('We could not found promotion id of the following payment id'.$aInfo['paymentID']);
            
            return false;
        }
        
        wp_update_post([
            'ID'          => $promotionID,
            'post_status' => 'publish'
        ]);
        
        return $promotionID;
    }
    
    public function removePromotionAfterPaymentRefunded($aInfo)
    {
        if (!isset($aInfo['category']) || $aInfo['category'] != $this->category) {
            return false;
        }
        
        $promotionID = PaymentMetaModel::getPromotionID($aInfo['paymentID']);
        
        if (!empty($promotionID)) {
            wp_update_post([
                'ID'          => $promotionID,
                'post_status' => 'trash'
            ]);
        }
    }
    
    /**
     * Using Stripe API v3: It's required in EU
     *
     * @see   https://stripe.com/docs/payments/checkout/server#create-one-time-payments
     * @since 1.1.7.6
     */
    public function createSession()
    {
        $aPaymentMethod = PaymentGatewayStaticFactory::get($this->gateway, $this->isNonRecurringPayment);
        if ($aPaymentMethod['status'] == 'success') {
            return $aPaymentMethod['oPaymentMethod']->proceedPayment($this->oReceipt);
        }
        
        return $aPaymentMethod;
    }
    
    public function boostListing()
    {
        $oRetrieve     = new RetrieveController(new AjaxRetrieve());
        $this->gateway = isset($_POST['gateway']) && !empty($_POST['gateway']) ? $_POST['gateway'] : 'woocommerce';
        
        $aMiddleware = ['isGatewaySupported', 'isSetupThankyouCancelUrl', 'isPublishedPost'];
        $status      = $this->middleware($aMiddleware, [
            'postID'    => $_POST['postID'],
            'gateway'   => $this->gateway,
            'isBoolean' => true
        ]);
        
        if (!$status) {
            return $oRetrieve->error(['msg' => 'You can promote a published listing only', 'wiloke-listing-tools']);
        }
        
        $noPlanMsg = esc_html__('You have to select 1 plan at least', 'wiloke-listing-tools');
        if (!isset($_POST['aPlans']) || empty($_POST['aPlans'])) {
            wp_send_json_error([
                'msg' => $noPlanMsg
            ]);
        }
        
        $aSelectedPlanKeys = [];
        $aSelectedPlans    = [];
        foreach ($_POST['aPlans'] as $aPlan) {
            if (isset($aPlan['value']) && $aPlan['value'] == 'yes') {
                $aSelectedPlanKeys[] = GetSettings::generateSavingPromotionDurationKey($aPlan);
                $aSelectedPlans[]    = $aPlan;
            }
        }
        
        if (empty($aSelectedPlanKeys)) {
            return $oRetrieve->error([
                'msg' => $noPlanMsg
            ]);
        }
        
        Session::setPaymentObjectID($_POST['postID']);
        
        if ($this->gateway == 'woocommerce') {
            $aProductIDs = [];
            
            foreach ($aSelectedPlans as $aPlan) {
                if (in_array(GetSettings::generateSavingPromotionDurationKey($aPlan), $aSelectedPlanKeys)) {
                    $aProductIDs[] = $aPlan['productAssociation'];
                }
            }
            
            if (empty($aProductIDs)) {
                return $oRetrieve->error(
                    [
                        'msg' => esc_html__('The product id is required', 'wiloke-listing-tools')
                    ]
                );
            }
            
            /*
			 * @WooCommerceController:removeProductFromCart
			 */
            do_action('wiloke-listing-tools/before-redirecting-to-cart', $aProductIDs);
            
            return $oRetrieve->success([
                'productIDs' => $aProductIDs,
                'cartUrl'    => wc_get_cart_url()
            ]);
        } else {
            $planName = sprintf(
                esc_html__('Promotion - %s', 'wiloke-listing-tools'),
                get_the_title($_POST['postID'])
            );
            
            $this->oReceipt = ReceiptStaticFactory::get($this->category, [
                'userID'            => User::getCurrentUserID(),
                'aSelectedPlanKeys' => $aSelectedPlanKeys,
                'planName'          => $planName,
                'gateway'           => $this->gateway,
                'couponCode'        => ''
            ]);
            
            $aStatus = $this->oReceipt->setupPlan();
            if ($aStatus['status'] == 'error') {
                return $oRetrieve->error($aStatus['msg']);
            }
            
            $aResponse = $this->createSession();
            
            if ($aResponse['status'] == 'error') {
                return $oRetrieve->error($aResponse);
            }
            
            return $oRetrieve->success($aResponse);
        }
    }
    
    public function getPaymentGateways()
    {
        $aPromotions = GetSettings::getOptions('promotion_plans');
        if (empty($aPromotions)) {
            wp_send_json_error();
        }
        
        foreach ($aPromotions as $aPromotion) {
            if (isset($aPromotion['productAssociation']) && !empty($aPromotion['productAssociation'])) {
                wp_send_json_error();
            }
        }
        
        $gateways = GetWilokeSubmission::getField('payment_gateways');
        if (empty($gateways)) {
            wp_send_json_error([
                'msg' => esc_html__('You do not have any gateways. Please go to Wiloke Submission to set one.')
            ]);
        }
        
        $aGatewayKeys  = explode(',', $gateways);
        $aGatewayNames = GetWilokeSubmission::getGatewaysWithName();
        
        $aGateways = [];
        foreach ($aGatewayKeys as $gateway) {
            $aGateways[$gateway] = $aGatewayNames[$gateway];
        }
        
        wp_send_json_success($aGateways);
    }
    
    public function fetchPromotions()
    {
        $aPromotions = GetSettings::getPromotionPlans();
        $currency    = GetWilokeSubmission::getField('currency_code');
        $symbol      = GetWilokeSubmission::getSymbol($currency);
        $position    = GetWilokeSubmission::getField('currency_position');
        
        $promotionID = GetSettings::getPostMeta($_POST['postID'], $this->belongsToPromotionKey);
        if (!empty($promotionID) && get_post_status($promotionID) === 'publish') {
            $now   = current_time('timestamp');
            $order = 0;
            foreach ($aPromotions as $key => $aPlanSetting) {
                // Listing Sidebar without id won't display
                if ($aPlanSetting['position'] == 'listing_sidebar' && empty($aPlanSetting['id'])) {
                    continue;
                }
                $aReturnPromotions[$order] = $aPlanSetting;
                $promotionExpiry           = GetSettings::getPostMeta($promotionID, 'promote_'.$key);
                if (!empty($promotionExpiry)) {
                    $promotionExpiry = abs($promotionExpiry);
                    if ($now < $promotionExpiry) {
                        $aReturnPromotions[$order]['isUsing'] = 'yes';
                        $convertHumanReadAble                 =
                            Time::toDateFormat($promotionExpiry).' '.Time::toTimeFormat($promotionExpiry);
                        $aReturnPromotions[$order]['expiry']  = sprintf(
                            esc_html__('Your Promotion will expiry on: %s', 'wiloke-listing-tools'),
                            $convertHumanReadAble
                        );
                    }
                }
                $order++;
            }
        } else {
            $aReturnPromotions = array_values($aPromotions);
            foreach ($aReturnPromotions as $key => $aPlanSetting) {
                // Listing Sidebar without id won't display
                if ($aPlanSetting['position'] == 'listing_sidebar' && empty($aPlanSetting['id'])) {
                    unset($aReturnPromotions[$key]);
                }
            }
        }
        
        wp_send_json_success(
            [
                'plans'    => $aReturnPromotions,
                'position' => $position,
                'symbol'   => $symbol
            ]
        );
    }
}

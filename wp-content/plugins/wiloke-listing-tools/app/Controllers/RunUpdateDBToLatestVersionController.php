<?php

namespace WilokeListingTools\Controllers;

use WilokeListingTools\AlterTable\AlterTablePaymentHistory;
use WilokeListingTools\AlterTable\AlterTablePaymentMeta;
use WilokeListingTools\Framework\Helpers\GetSettings;
use WilokeListingTools\Framework\Helpers\SetSettings;
use WilokeListingTools\Framework\Routing\Controller;
use WilokeListingTools\Framework\Store\Session;
use WilokeListingTools\Models\PaymentMetaModel;

class RunUpdateDBToLatestVersionController extends Controller
{
    private $optionKey = 'updated_db';
    private $nonceAction = 'wilcity_update_db_nonce';
    private $action = 'wilcity_update_db';
    
    public function __construct()
    {
        add_action('admin_init', [$this, 'convertingStripeSubscriptionIDToSubscriptionID']);
        add_action('admin_notices', [$this, 'requireUpdateAnnouncement']);
    }

    public function requireUpdateAnnouncement()
    {
        $updatedDBVersion = GetSettings::getOptions($this->optionKey);
        
        if ($updatedDBVersion != WILOKE_LISTING_TOOL_VERSION):
            $url = add_query_arg(
                [
                    'page'     => 'wiloke-submission',
                    'security' => wp_create_nonce($this->nonceAction),
                    'action'   => $this->action
                ],
                admin_url('admin.php')
            );
            ?>
            <div class="notice notice-error is-dismissible" style="margin-top: 20px; margin-bottom: 20px">
                <p><strong style="color: red">Wiloke Submission update - We need to update your Wiloke Submission
                        database to the latest
                    version</strong></p>
                <a href="<?php echo esc_url($url); ?>">Run the updater</a>
            </div>
            <?php
        else:
            if (Session::getSession('updated_db') == 'yes') :
                ?>
                <div class="notice notice-success is-dismissible" style="margin-top: 20px; margin-bottom: 20px">
                    <p>Thank for updating to the latest version of Wiloke Listing Tools</p>
                </div>
                <?php
            endif;
        endif;
    }
    
    private function convertStripeSubscriptionIDToSubscriptionID()
    {
        global $wpdb;
        $paymentTbl = $wpdb->prefix.AlterTablePaymentHistory::$tblName;
        
        $aStripePaymentIDs = $wpdb->get_results(
            $wpdb->prepare(
                "SELECT * FROM $paymentTbl WHERE gateway=%s AND billingType=%s ORDER BY ID DESC LIMIT 3000",
                'stripe', 'RecurringPayment'
            ),
            ARRAY_A
        );
        
        if (!empty($aStripePaymentIDs)) {
            foreach ($aStripePaymentIDs as $aPayment) {
                $stripeSubscriptionID = PaymentMetaModel::get($aPayment['ID'], 'stripe_subscription_ID');
                if (!empty($stripeSubscriptionID)) {
                    PaymentMetaModel::setPaymentSubscriptionID($aPayment['ID'], $stripeSubscriptionID);
                    PaymentMetaModel::delete($aPayment['ID'], 'stripe_subscription_ID');
                }
            }
        }
        
        return true;
    }
    
    private function convertPayPalTokenAndStoreDataToPaymentTokenID()
    {
        global $wpdb;
        $paymentTbl = $wpdb->prefix.AlterTablePaymentHistory::$tblName;
        
        $aPaymentIDs = $wpdb->get_results(
            $wpdb->prepare(
                "SELECT * FROM $paymentTbl WHERE gateway=%s ORDER BY ID DESC LIMIT 3000",
                'paypal'
            ),
            ARRAY_A
        );
        
        if (!empty($aPaymentIDs)) {
            foreach ($aPaymentIDs as $aPayment) {
                $token = PaymentMetaModel::get($aPayment['ID'], 'paypal_token_id_relationship');
                if (!empty($token)) {
                    PaymentMetaModel::setPaymentToken($aPayment['ID'], $token);
                    PaymentMetaModel::delete($aPayment['ID'], 'paypal_token_id_relationship');
                }
            }
        }
        
        return true;
    }
    
    private function convertPayPalPaymentIDToIntentID()
    {
        global $wpdb;
        $paymentTbl = $wpdb->prefix.AlterTablePaymentHistory::$tblName;
        
        $aPaymentIDs = $wpdb->get_results(
            $wpdb->prepare(
                "SELECT * FROM $paymentTbl WHERE gateway=%s AND billingType = %s ORDER BY ID DESC LIMIT 3000",
                'paypal', 'NonRecurringPayment'
            ),
            ARRAY_A
        );
        
        if (!empty($aPaymentIDs)) {
            foreach ($aPaymentIDs as $aPayment) {
                $intentID = PaymentMetaModel::get($aPayment['ID'], 'paypal_payment_id');
                if (!empty($intentID)) {
                    PaymentMetaModel::setPaymentIntentID($aPayment['ID'], $intentID);
                    PaymentMetaModel::delete($aPayment['ID'], 'paypal_payment_id');
                }
            }
        }
        
        return true;
    }
    
    public function convertingStripeSubscriptionIDToSubscriptionID()
    {
        if (isset($_REQUEST['action']) && $_REQUEST['action'] == $this->action) {
            if (wp_verify_nonce($_REQUEST['security'], $this->nonceAction)) {
                if (current_user_can('administrator')) {
                    $this->convertStripeSubscriptionIDToSubscriptionID();
                    $this->convertPayPalTokenAndStoreDataToPaymentTokenID();
                    $this->convertPayPalPaymentIDToIntentID();
                    
                    SetSettings::setOptions($this->optionKey, WILOKE_LISTING_TOOL_VERSION);
                    Session::setSession('updated_db', 'yes');
                }
            }
        }
    }
}

<?php

namespace WilokeListingTools\Controllers;

use WilokeListingTools\Framework\Helpers\GetSettings;
use WilokeListingTools\Framework\Helpers\GetWilokeSubmission;
use WilokeListingTools\Framework\Routing\Controller;
use WilokeListingTools\Framework\Store\Session;
use WilokeListingTools\Frontend\User;
use WilokeListingTools\Models\InvoiceModel;
use WilokeListingTools\Models\PaymentMetaModel;
use WilokeListingTools\Models\PaymentModel;
use WilokeListingTools\Models\PlanRelationshipModel;
use WilokeListingTools\Models\RemainingItems;

class UserController extends Controller
{
    public $limit = 4;
    
    public function __construct()
    {
        add_action('wp_ajax_wilcity_fetch_user_profile', [$this, 'fetchUserProfile']);
        add_action('admin_init', [$this, 'addCaps']);
        add_action('ajax_query_attachments_args', [$this, 'mediaAccess']);
        add_action('wp_ajax_signin_firebase', [$this, 'signinFirebase']);
        add_filter('manage_users_columns', [$this, 'registerAddlistingLockedColumn']);
        add_filter('manage_users_custom_column', [$this, 'showUpLockedUserReasonOnUserRow'], 10, 3);
        add_action('wp_ajax_wilcity_fetch_my_billings', [$this, 'fetchBillings']);
        add_action('wp_ajax_wilcity_fetch_my_billing_details', [$this, 'fetchBillingDetails']);
        add_action('wp_ajax_wilcity_fetch_my_plan', [$this, 'fetchMyPlan']);
    }
    
    public function fetchMyPlan()
    {
        $aRawUserPlans = PaymentModel::getPaymentSessionsOfUser(User::getCurrentUserID(), ['active', 'succeeded']);
        
        if (empty($aRawUserPlans)) {
            wp_send_json_error([
                'msg' => esc_html__('You have not used any plan.', 'wiloke-listing-tools')
            ]);
        }
        
        $aUserPlans = [];
        $order      = 0;
        foreach ($aRawUserPlans as $oPayment) {
            $aPaymentInfo = PaymentMetaModel::getPaymentInfo($oPayment->ID);
            
            if (!empty($oPayment->planID)) {
                $planTitle = get_the_title($oPayment->planID);
            }
            
            if (empty($planTitle) && isset($aPaymentInfo['planName'])) {
                $planTitle = $aPaymentInfo['planName'];
            }
            
            if (empty($planTitle)) {
                $planTitle = esc_html__('This plan may have been deleted.', 'wiloke-listing-tools');
            }
            
            $aUserPlans[$order]['planName'] = $planTitle;
            $aUserPlans[$order]['planID']   = $oPayment->planID;
            
            if (GetWilokeSubmission::isNonRecurringPayment($oPayment->billingType)) {
                $aUserPlans[$order]['nextBillingDate'] = 'X';
            } else {
                $nextBillingDateGMT = PaymentMetaModel::getNextBillingDateGMT($oPayment->ID);
                if (empty($nextBillingDateGMT)) {
                    $aUserPlans[$order]['nextBillingDate'] = esc_html__('Updating', 'wiloke-listing-tools');
                } else {
                    $aUserPlans[$order]['nextBillingDate'] = date_i18n(get_option('date_format'), $nextBillingDateGMT);
                }
            }
            
            $aUserPlans[$order]['paymentID'] = $oPayment->ID;
            $aUserPlans[$order]['gateway']   = $oPayment->gateway;
            
            if (in_array($aPaymentInfo['category'], ['addlisting', 'paidClaim'])) {
                $listingID                      = PlanRelationshipModel::getLastObjectIDByPaymentID($oPayment->ID);
                $aUserPlans[$order]['postType'] = get_post_type($listingID);
            } else {
                $aUserPlans[$order]['postType'] = $aPaymentInfo['category'];
            }
            
            $aUserPlans[$order]['billingType']           = $oPayment->billingType;
            $aUserPlans[$order]['isNonRecurringPayment'] =
                GetWilokeSubmission::isNonRecurringPayment($oPayment->billingType) ? 'yes' : 'no';
            $aUserPlans[$order]['status']                = $oPayment->status;
            $aUserPlans[$order]['category']              = $aPaymentInfo['category'];
            
            if (in_array($aPaymentInfo['category'], ['addlisting', 'paidClaim'])) {
                $oRemainingItems = new RemainingItems();
                $oRemainingItems->setUserID($oPayment->userID)->setGateway($oPayment->gateway)->setPlanID
                ($oPayment->planID)->setBillingType($oPayment->billingType)->setPaymentID($oPayment->ID)
                ;
                
                $aUserPlans[$order]['remainingItems'] = $oRemainingItems->getRemainingItems();
            } else {
                $aUserPlans[$order]['remainingItems'] = 'x';
            }
            
            $order++;
        }
        
        wp_send_json_success($aUserPlans);
    }
    
    public function fetchBillingDetails()
    {
        $aResult = InvoiceModel::getInvoiceDetails($_POST['invoiceID']);
        if (empty($aResult)) {
            wp_send_json_error([
                'msg' => esc_html__('This plan may have been deleted', 'wiloke-listing-tools')
            ]);
        }
        
        wp_send_json_success($aResult);
    }
    
    public function fetchBillings()
    {
        $offset = (abs($_POST['page']) - 1) * $this->limit;
        
        $aInvoices = InvoiceModel::getMyInvoices($this->limit, $offset);
        if (empty($aInvoices)) {
            if ($_POST['page'] > 1) {
                wp_send_json_error([
                    'reachedMaximum' => 'yes'
                ]);
            } else {
                wp_send_json_error(['msg' => esc_html__('There are no invoices', 'wiloke-listing-tools')]);
            }
        }
        wp_send_json_success($aInvoices);
    }
    
    public function registerAddlistingLockedColumn($aColumns)
    {
        $aColumns['addlisting_locked'] = 'Locked Status';
        
        return $aColumns;
    }
    
    public function showUpLockedUserReasonOnUserRow($val, $columnName, $userID)
    {
        switch ($columnName) {
            case 'addlisting_locked' :
                $val = GetSettings::getUserMeta($userID, 'locked_addlisting');
                break;
        }
        
        return $val;
    }
    
    public function signinFirebase()
    {
        if (!is_user_logged_in()) {
            wp_send_json_error();
        }
        
        wp_send_json_success(
            [
                'email'    => User::getField('user_email', get_current_user_id()),
                'password' => User::getField('user_pass', get_current_user_id())
            ]
        );
    }
    
    public function addCaps()
    {
        $oContributor = get_role('contributor');
        $oContributor->add_cap('upload_files');
        
        if (class_exists('\WilokeThemeOptions') &&
            \WilokeThemeOptions::getOptionDetail('addlisting_upload_img_via') == 'wp'
        ) {
            $oSubscriber = get_role('subscriber');
            if (!empty($oSubscriber)) {
                if (current_user_can('subscriber')) {
                    $oSubscriber->add_cap('upload_files');
                } else {
                    $oSubscriber->remove_cap('upload_files');
                }
            }
        }
    }
    
    public function mediaAccess($aArgs)
    {
        $userID = User::getCurrentUserID();
        if (!empty($userID) && class_exists('\WilokeThemeOptions')) {
            if (\WilokeThemeOptions::isEnable('user_admin_access_all_media',
                    true) && User::currentUserCan('administrator')
            ) {
                return $aArgs;
            }
            
            $aArgs['author'] = User::getCurrentUserID();
        }
        
        return $aArgs;
    }
    
    public function fetchUserProfile()
    {
        $this->middleware(['isUserLoggedIn'], []);
        $userID = get_current_user_id();
        
        $aThemeOptions = \Wiloke::getThemeOptions();
        
        wp_send_json_success([
            'display_name'        => User::getField('display_name', $userID),
            'avatar'              => User::getAvatar($userID),
            'position'            => User::getPosition($userID),
            'profile_description' => isset($aThemeOptions['dashboard_profile_description']) ?
                $aThemeOptions['dashboard_profile_description'] : '',
            'author_url'          => get_author_posts_url($userID)
        ]);
    }
}

<?php

namespace WilokeListingTools\Controllers;

use WilokeListingTools\Framework\Helpers\DebugStatus;
use WilokeListingTools\Framework\Helpers\FileSystem;
use WilokeListingTools\Framework\Helpers\GetSettings;
use WilokeListingTools\Framework\Helpers\GetWilokeSubmission;
use WilokeListingTools\Framework\Helpers\SetSettings;
use WilokeListingTools\Framework\Helpers\Submission;
use WilokeListingTools\Framework\Payment\FreePlan\FreePlan;
use WilokeListingTools\Framework\Payment\Receipt\ReceiptStaticFactory;
use WilokeListingTools\Framework\Store\Session;
use WilokeListingTools\Frontend\User;
use WilokeListingTools\Models\RemainingItems;
use WilokeListingTools\Models\UserModel;
use WilokeListingTools\Register\WilokeSubmission;

trait HandleSubmit
{
    private $oReceipt;
    private $isChangedPlan;
    private $aMaybeUserPlan = [];
    
    private function isSentEmailNotificationAboutSubmission($listingID)
    {
        return get_transient('wilcity_is_sent_email_nas_'.$listingID);
    }
    
    private function setSentEmailNotificationAboutSubmission($listingID)
    {
        set_transient('wilcity_is_sent_email_nas_'.$listingID, 'yes',
            apply_filters('wilcity/stop-sending-email-within', 60 * 24));
    }
    
    private function submittingListingHookPlace()
    {
        /**
         * @hooked WilokeListingTools\Controllers\EmailController::submittedListing 10
         * @hooked WilokeListingTools\Controllers\PlanRelationshipController::addPlanRelationshipUserPurchasedPlan 10
         * @hooked WilokeListingTools\Controllers\PostController::handleListingPlanAfterSubmitting 15
         * @hooked WilokeListingTools\Controllers\SessionController::maybeDeletePaymentSessions 15
         */
        do_action(
            'wiloke/submitted-listing',
            [
                'planID'         => $this->planID,
                'postID'         => $this->listingID,
                'postAuthor'     => get_post_field('post_author', $this->listingID),
                'isAutoApproved' => false,
                'isChangedPlan'  => $this->isChangedPlan,
                'aUserPlan'      => $this->aMaybeUserPlan
            ]
        );
    }
    
    private function goToThankyouPage()
    {
        wp_send_json_success(
            [
                'redirectTo' => GetWilokeSubmission::getThankyouPageURL(
                    [
                        'postID'   => $this->listingID,
                        'category' => 'addlisting'
                    ]
                )
            ]
        );
    }
    
    private function _handleSubmit()
    {
        $this->middleware(
            [
                'isLockedAddListing'
            ]
        );
        
        $aUserPlan = [];
        Session::setPaymentCategory('addlisting');
        
        $this->listingID = Session::getPaymentObjectID(false);
        Session::setSession(wilokeListingToolsRepository()->get('payment:listingType'),
            get_post_type($this->listingID));
        
        $this->postStatus    = get_post_status($this->listingID);
        $this->planID        = Session::getSession(wilokeListingToolsRepository()->get('payment:storePlanID'));
        $this->aPlanSettings = GetSettings::getPlanSettings($this->planID);
        
        $oldPlanID           = GetSettings::getPostMeta($this->listingID, 'oldPlanID');
        $this->isChangedPlan = !empty($oldPlanID) && $oldPlanID != $this->planID;
        
        $this->middleware([
            'isUserLoggedIn',
            'canSubmissionListing',
            'isPassedPostAuthor',
            'isExceededFreePlan',
            'isPlanExists'
        ], [
            'postID'      => $this->listingID,
            'planID'      => $this->planID,
            'listingID'   => $this->listingID,
            'userID'      => get_current_user_id(),
            'listingType' => get_post_type($this->listingID)
        ]);
        
        $aUpdatePost = [
            'ID' => $this->listingID
        ];
        
        if (!defined('WILOKE_LISTING_TOOLS_CHECK_EVEN_ADMIN') && current_user_can('administrator')) {
            $aUpdatePost['post_status'] = 'publish';
            $this->setDuration(GetWilokeSubmission::getBillingType(), $this->listingID, $this->planID);
            wp_update_post($aUpdatePost);
            $this->goToThankyouPage();
        }
        
        if ($this->postStatus == 'editing') {
            if (GetWilokeSubmission::getField('published_listing_editable') == 'allow_trust_approved') {
                $aUpdatePost['post_status'] = 'publish';
            } else {
                $oldPostStatus = GetSettings::getPostMeta($this->listingID, 'oldPostStatus');
                if (Submission::listingStatusWillPublishImmediately($oldPostStatus)) {
                    $aUpdatePost['post_status'] = 'publish';
                } else {
                    $aUpdatePost['post_status'] = 'pending';
                }
                $aUpdatePost['post_status'] = 'pending';
                SetSettings::deletePostMeta($oldPostStatus, 'oldPostStatus');
            }
            
            if ($aUpdatePost['post_status'] != 'publish' &&
                !$this->isSentEmailNotificationAboutSubmission($this->listingID)
            ) {
                $this->submittingListingHookPlace();
                $this->setSentEmailNotificationAboutSubmission($this->listingID);
            }
            
            wp_update_post($aUpdatePost);
            $this->goToThankyouPage();
        }
        
        if (in_array($this->postStatus, ['unpaid', 'expired'])) {
            $aUserPlan      = UserModel::getSpecifyUserPlanID($this->planID, User::getCurrentUserID(), true);
            
            if (UserModel::getRemainingItemsOfPlans($this->planID) && (!defined('WILOKE_ALWAYS_PAY') || !WILOKE_ALWAYS_PAY)) {
                $this->aMaybeUserPlan = $aUserPlan;
                $isTrial              = isset($aUserPlan['isTrial']) && $aUserPlan['isTrial'];
                $this->setDuration(GetWilokeSubmission::getBillingType(), $this->listingID, $this->planID,
                    $isTrial);
                $oldPostStatus = GetSettings::getPostMeta($this->listingID, 'oldPostStatus');
                
                if (
                    GetWilokeSubmission::getField('approved_method') == 'auto_approved_after_payment'
                    || Submission::listingStatusWillPublishImmediately($oldPostStatus)
                ) {
                    $aUpdatePost['post_status'] = 'publish';
                } else {
                    $aUpdatePost['post_status'] = 'pending';
                }
                
                wp_update_post($aUpdatePost);
                SetSettings::deletePostMeta($oldPostStatus, 'oldPostStatus');
                $this->submittingListingHookPlace();
                $this->goToThankyouPage();
            }
//            else if (GetWilokeSubmission::isNonRecurringPayment($aUserPlan['billingType'])) {
//                $this->middleware(['isExceededMaximumListing'], ['aUserPlan' => $aUserPlan]);
//            }
        }
        
        // Free Add Listing
        if (empty($this->aPlanSettings['regular_price'])) {
            $this->oReceipt = ReceiptStaticFactory::get('addlisting', [
                'planID'     => $this->planID,
                'userID'     => User::getCurrentUserID(),
                'couponCode' => '',
                'aRequested' => $_REQUEST
            ]);
            $this->oReceipt->setupPlan();
            
            $oFreePlan = new FreePlan($this->planID);
            $aStatus   = $oFreePlan->proceedPayment($this->oReceipt);
            
            if ($aStatus['status'] == 'success') {
                $isTrial = isset($aUserPlan['isTrial']) && $aUserPlan['isTrial'];
                $this->setDuration(GetWilokeSubmission::getBillingType(), $this->listingID, $this->planID, $isTrial);
                
                wp_update_post([
                    'ID'          => $this->listingID,
                    'post_status' => GetWilokeSubmission::getField('approved_method') != 'manual_review' ? 'publish' :
                        'pending'
                ]);
                
                $this->submittingListingHookPlace();
                $this->goToThankyouPage();
            } else {
                wp_send_json_error([
                    'msg' => esc_html__('ERROR: We could not create Free Plan', 'wiloke-listing-tools')
                ]);
            }
        }
        
        $redirectTo = GetWilokeSubmission::getField('checkout', true);
        // If paying via WooCoomerce, we need to get rid of this product id from the cart
        $productID = GetSettings::getPostMeta($this->planID, 'woocommerce_association');
        
        if (!empty($productID)) {
            $redirectTo = GetSettings::getCartUrl($this->planID);
            /*
            * @hooked WooCommerceController:removeProductFromCart
            */
            do_action('wiloke-listing-tools/before-redirecting-to-cart', $productID);
            //            Session::setSession(wilokeListingToolsRepository()->get('payment:associateProductID'), $productID);
            Session::setProductID($productID);
        }
        
        $this->submittingListingHookPlace();
        
        wp_send_json_success(
            [
                'redirectTo' => $redirectTo
            ]
        );
    }
}

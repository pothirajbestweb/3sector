<?php
namespace WilokeListingTools\Framework\Store;

use WilokeListingTools\Framework\Helpers\DebugStatus;

class Session
{
    protected static $isSessionStarted = false;
    protected static $expiration = 900;
    
    protected static function generatePrefix($name)
    {
        return wilokeListingToolsRepository()->get('general:prefix').$name;
    }
    
    protected static function sessionStart($sessionID = null)
    {
        if (!empty($sessionID)) {
            session_id($sessionID);
        }
        
        if (!headers_sent() && session_status() == PHP_SESSION_NONE){
            session_start();
        }
    }
    
    public static function getSessionID()
    {
        session_start();
        var_export(session_id());
    }
    
    public static function setSession($name, $value, $sessionID = null)
    {
        $value = maybe_serialize($value);
        if (DebugStatus::status('WILOKE_STORE_WITH_DB')) {
            set_transient(self::generatePrefix($name), $value, self::$expiration);
        } else {
            if (empty(session_id())) {
                self::sessionStart($sessionID);
            }
            $_SESSION[self::generatePrefix($name)] = $value;
        }
    }
    
    public static function getSession($name, $thenDestroy = false)
    {
        if (DebugStatus::status('WILOKE_STORE_WITH_DB')) {
            $value = get_transient(self::generatePrefix($name));
        } else {
            self::sessionStart();
            $value = isset($_SESSION[self::generatePrefix($name)]) ? $_SESSION[self::generatePrefix($name)] : '';
        }
        
        if (empty($value)) {
            return false;
        }
        
        if ($thenDestroy) {
            self::destroySession($name);
        }
        
        return maybe_unserialize($value);
    }
    
    public static function deleteAllSessions()
    {
        self::sessionStart();
        
        Session::destroySession(wilokeListingToolsRepository()->get('payment:sessionObjectStore'));
        Session::destroySession(wilokeListingToolsRepository()->get('payment:storePlanID'));
        Session::destroySession(wilokeListingToolsRepository()->get('addlisting:isAddingListingSession'));
        Session::destroySession(wilokeListingToolsRepository()->get('payment:sessionRelationshipStore'));
        Session::destroySession(wilokeListingToolsRepository()->get('payment:sessionObjectStore'));
    }
    
    public static function destroySession($name = null)
    {
        self::sessionStart();
        if (DebugStatus::status('WILOKE_STORE_WITH_DB')) {
            delete_transient(self::generatePrefix($name));
        } else {
            if (!empty(self::generatePrefix($name))) {
                unset($_SESSION[self::generatePrefix($name)]);
            } else {
                session_destroy();
            }
        }
    }
    
    /**
     * @param bool $thenDestroy
     *
     * @return bool|mixed
     */
    public static function getPaymentPlanID($thenDestroy = false)
    {
       return Session::getSession(wilokeListingToolsRepository()->get('payment:storePlanID'), $thenDestroy);
    }
    
    /**
     * @param bool $thenDestroy
     *
     * @return bool|mixed
     */
    public static function getPaymentObjectID($thenDestroy = false)
    {
        return Session::getSession(wilokeListingToolsRepository()->get('payment:sessionObjectStore'), $thenDestroy);
    }
    
    /**
     * @param bool $thenDestroy
     *
     * @return bool|mixed
     */
    public static function getPaymentCategory($thenDestroy = false)
    {
        return Session::getSession(wilokeListingToolsRepository()->get('payment:category'), $thenDestroy);
    }
    
    /**
     * @param bool $thenDestroy
     *
     * @return bool|mixed
     */
    public static function getProductID($thenDestroy = false)
    {
        return Session::getSession(wilokeListingToolsRepository()->get('payment:associateProductID'), $thenDestroy);
    }
    
    /**
     * @param bool $thenDestroy
     *
     * @return bool|mixed
     */
    public static function getClaimID($thenDestroy = false)
    {
        return Session::getSession(wilokeListingToolsRepository()->get('claim:sessionClaimID'), $thenDestroy);
    }
    
    public static function getPaymentID($theyDestroy = false)
    {
        return Session::getSession(wilokeListingToolsRepository()->get('payment:paymentID'), $theyDestroy);
    }
    
    public static function setPaymentPlanID($planID)
    {
        Session::setSession(wilokeListingToolsRepository()->get('payment:storePlanID'), $planID);
    }
    
    /**
     * @param $listingID
     */
    public static function setPaymentObjectID($listingID)
    {
        Session::setSession(wilokeListingToolsRepository()->get('payment:sessionObjectStore'), $listingID);
    }
    
    /**
     * @param $category
     */
    public static function setPaymentCategory($category)
    {
        Session::setSession(wilokeListingToolsRepository()->get('payment:category'), $category);
    }
    
    /**
     * @param $productID
     */
    public static function setProductID($productID)
    {
        Session::setSession(wilokeListingToolsRepository()->get('payment:associateProductID'), $productID);
    }
    
    /**
     * @param $paymentID
     */
    public static function setPaymentID($paymentID)
    {
        Session::setSession(wilokeListingToolsRepository()->get('payment:paymentID'), $paymentID);
    }
    
    /**
     * @param $claimID
     */
    public static function setClaimID($claimID)
    {
        Session::setSession(wilokeListingToolsRepository()->get('claim:sessionClaimID'), $claimID);
    }
}

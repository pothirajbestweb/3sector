<?php
namespace WilokeListingTools\Framework\Payment\PayPal;

use PayPal\Api\Amount;
use PayPal\Api\Details;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\RedirectUrls;
use PayPal\Api\Transaction;
use PayPal\Exception\PayPalConnectionException;

use WilokeListingTools\Controllers\Retrieve\NormalRetrieve;
use WilokeListingTools\Controllers\RetrieveController;
use WilokeListingTools\Framework\Helpers\FileSystem;
use WilokeListingTools\Framework\Payment\CreatedPaymentHook;
use WilokeListingTools\Framework\Payment\PaymentMethodInterface;
use WilokeListingTools\Framework\Payment\PayPalPayment;
use WilokeListingTools\Framework\Payment\Receipt;
use WilokeListingTools\Framework\Store\Session;
use WilokeListingTools\Models\PaymentMetaModel;
use WilokeListingTools\Models\PaymentModel;

final class PayPalNonRecurringPaymentMethod extends PayPalPayment implements PaymentMethodInterface
{
    public function getBillingType()
    {
        return wilokeListingToolsRepository()->get('payment:billingTypes', true)->sub('nonrecurring');
    }
    
    private function getApprovalUrl()
    {
        // Create new payer and method
        $oPayer = new Payer();
        $oPayer->setPaymentMethod('paypal');
        $oRetrieve = new RetrieveController(new NormalRetrieve());
        
        try {
            $oInstPlan = new Item();
            $oInstPlan->setName($this->oReceipt->getPlanName())
                      ->setCurrency($this->oReceipt->getCurrency())
                      ->setQuantity(1)
                      ->setPrice($this->oReceipt->getSubTotal())
            ;
            $aItems[] = $oInstPlan;
        } catch (\Exception $oE) {
            return $oRetrieve->error($oE->getMessage());
        };
        
        if (!empty($this->oReceipt->getDiscount())) {
            // Maybe discount
            $discountPrice = '-'.$this->oReceipt->getDiscount();
            settype($discountPrice, 'string');
            
            try {
                $oDiscount = new Item();
                $oDiscount->setName($this->oReceipt->getPlanName())
                          ->setCurrency($this->oReceipt->getCurrency())
                          ->setQuantity(1)
                          ->setPrice($discountPrice)
                ;
                $aItems[] = $oDiscount;
            } catch (\Exception $oE) {
                return $oRetrieve->error($oE->getMessage());
            }
        }
        
        try {
            $oItemList = new ItemList();
            $oItemList->setItems($aItems);
        } catch (\Exception $oE) {
            return $oRetrieve->error($oE->getMessage());
        }
        
        try {
            $instDetails = new Details();
            $instDetails->setSubtotal($this->oReceipt->getTotal());
        } catch (\Exception $oE) {
            return $oRetrieve->error($oE->getMessage());
        }
        
        try {
            $instAmount = new Amount();
            $instAmount->setCurrency($this->oReceipt->getCurrency())
                       ->setTotal($this->oReceipt->getTotal())
                       ->setDetails($instDetails)
            ;
        } catch (\Exception $oE) {
            return $oRetrieve->error($oE->getMessage());
        }
        
        // Set transaction object
        try {
            $transaction = new Transaction();
            $transaction->setItemList($oItemList)
                        ->setAmount($instAmount)
                        ->setInvoiceNumber(uniqid())
                        ->setDescription($this->paymentDescription)
            ;
        } catch (\Exception $oE) {
            return $oRetrieve->error($oE->getMessage());
        }
        
        // Set redirect urls
        try {
            $oRedirectUrls = new RedirectUrls();
            $oRedirectUrls->setReturnUrl($this->thankyouUrl($this->getBillingType()))
                          ->setCancelUrl($this->cancelUrl($this->getBillingType()))
            ;
        } catch (\Exception $oE) {
            return $oRetrieve->error($oE->getMessage());
        }
        
        // Create the full payment object
        try {
            $oPayment = new Payment();
            $oPayment->setIntent('sale')
                     ->setPayer($oPayer)
                     ->setRedirectUrls($oRedirectUrls)
                     ->setTransactions([$transaction])
            ;
        } catch (\Exception $oE) {
            return $oRetrieve->error($oE->getMessage());
        }
        // Create payment with valid API context
        
        try {
            $oPayment->create($this->oApiContext);
            // Get PayPal redirect URL and redirect user
            $approvalUrl = $oPayment->getApprovalLink();
            $this->parseTokenFromApprovalUrl($approvalUrl);
            
            $oAddPaymentHook = new CreatedPaymentHook(new PayPalNonRecurringCreatedPaymentHook($this));
            $oAddPaymentHook->doSuccess();
            
            $this->paymentID = PaymentMetaModel::getPaymentIDByToken($this->token);
            if (empty($this->paymentID)) {
                FileSystem::logError('We could not create payment id');
                
                return $oRetrieve->error([
                    'msg' => esc_html__('Could not insert Payment History', 'wiloke-listing-tools')
                ]);
            }
            
            FileSystem::logSuccess(
                'We got PayPal Approval URL. PaymentID:'.$this->paymentID.' Approval URL:'.$approvalUrl,
                __CLASS__,
                __METHOD__
            );
            
            Session::setSession('waiting_for_paypal_execution', 'yes');
    
            return $oRetrieve->success([
                'msg'        => esc_html__('The payment has been created successfully. We will redirect to PayPal shortly', 'wiloke-listing-tools'),
                'redirectTo' => $approvalUrl,
                'paymentID'  => $this->paymentID,
                'gateway'    => $this->gateway
            ]);
        } catch (PayPalConnectionException $ex) {
            FileSystem::logError($ex->getMessage(), __CLASS__, __METHOD__);
            
            return $oRetrieve->error([
                'code'   => $ex->getCode(),
                'status' => 'error',
                'msg'    => $ex->getMessage()
            ]);
        } catch (\Exception $ex) {
            FileSystem::logError($ex->getMessage(), __CLASS__, __METHOD__);
            
            return $oRetrieve->error([
                'status' => 'error',
                'msg'    => $ex->getMessage()
            ]);
        }
    }
    
    /**
     * @param Receipt\ReceiptStructureInterface $oReceipt
     *
     * @return array
     */
    public function proceedPayment(Receipt\ReceiptStructureInterface $oReceipt)
    {
        $this->oReceipt = $oReceipt;
        $this->setup();
        
        return $this->getApprovalUrl();
    }
    
    public function __get($name)
    {
        if (property_exists($this, $name)) {
            return $this->$name;
        } else {
            FileSystem::logError('Stripe: The property '.$name.' does not exist');
            
            return false;
        }
    }
    
    public function __isset($name)
    {
        return !empty($this->$name);
    }
}

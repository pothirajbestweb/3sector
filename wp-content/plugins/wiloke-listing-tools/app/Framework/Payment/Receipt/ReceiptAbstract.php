<?php

namespace WilokeListingTools\Framework\Payment\Receipt;

use WilokeListingTools\Framework\Helpers\GetWilokeSubmission;
use WilokeListingTools\Framework\Store\Session;

abstract class ReceiptAbstract
{
    protected $gateway;
    protected $couponCode;
    protected $userID;
    protected $planID;
    protected $orderID;
    protected $productID;
    protected $aInfo;
    protected $aPlanSettings;
    protected $aCouponInfo;
    protected $discountPrice = 0;
    protected $subTotal = 0;
    protected $isWooCommerce = false;
    protected $category;
    
    public function getCurrency()
    {
        return $this->isWooCommerce ? get_woocommerce_currency() : GetWilokeSubmission::getField('currency_code');
    }
    
    public function getThankyouURL($aArgs = [])
    {
        return GetWilokeSubmission::getThankyouPageURL($aArgs);
    }
    
    public function getCancelUrl($aArgs = [])
    {
        return GetWilokeSubmission::getCancelPageURL($aArgs);
    }
    
    public function getProductID()
    {
        return $this->productID;
    }
    
    public function getOrderID()
    {
        return $this->orderID;
    }
    
    public function roundPrice($price)
    {
        return round(trim($price), 2);
    }
    
    public function getPlanID()
    {
        return $this->planID;
    }
    
    protected function setUserID($userID)
    {
        $this->userID = $userID;
    }
    
    public function getUserID()
    {
        return $this->userID;
    }
    
    public function getDiscount()
    {
        return $this->discountPrice;
    }
    
    public function getPackageType()
    {
        return $this->category;
    }
    
    public function getPaymentData($aAdditionalData = [])
    {
        // TODO: Implement getPaymentData() method.
    }
    
    public function getSubTotal()
    {
        return $this->subTotal;
    }
    
    public function getTotal()
    {
        return $this->getSubTotal() - $this->discountPrice;
    }
    
    protected function setupCategorySession()
    {
        Session::setPaymentCategory($this->category);
    }
    
    public function getGateway()
    {
        return $this->gateway;
    }
    
    public function getTax()
    {
        return apply_filters('wilcity/wiloke-listing-tools/receipt/tax', 0, $this);
    }
}

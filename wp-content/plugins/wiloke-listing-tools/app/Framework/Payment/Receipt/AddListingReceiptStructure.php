<?php
namespace WilokeListingTools\Framework\Payment\Receipt;

use WilokeListingTools\Framework\Helpers\GetSettings;
use WilokeListingTools\Framework\Helpers\GetWilokeSubmission;
use WilokeListingTools\Framework\Helpers\Message;
use WilokeListingTools\Framework\Payment\Coupon;

final class AddListingReceiptStructure extends ReceiptAbstract implements ReceiptStructureInterface
{
    protected $category = 'addlisting';
    /**
     * @var \WooCommerce
     */
    private $oProduct;
    
    public function __construct($aInfo)
    {
        $this->aInfo = $aInfo;
    }
    
    /**
     * @return string
     */
    public function getPlanSlug()
    {
        return get_post_field('post_name', $this->planID);
    }
    
    private function setCouponCode()
    {
        $this->couponCode = isset($this->aInfo['couponCode']) ? $this->aInfo['couponCode'] : '';
    }
    
    public function getPlanName()
    {
        return get_the_title($this->planID);
    }
    
    public function getPlanFeaturedImg()
    {
        return get_the_post_thumbnail_url($this->planID, 'large');
    }
    
    public function getPlanDescription()
    {
        $desc = get_post_field('post_excerpt', $this->planID);
        if (empty($desc)) {
            $desc = $this->getPlanName();
        }
        
        return $desc;
    }
    
    public function getSubTotal()
    {
        if (!empty($this->subTotal)) {
            return $this->subTotal;
        }
    
        if ($this->isWooCommerce) {
            $this->subTotal = $this->oProduct->get_price();
        } else {
            $this->subTotal = $this->aPlanSettings['regular_price'];
        }
        
        $this->subTotal = $this->roundPrice($this->subTotal);
        
        return $this->subTotal;
    }
    
    public function getTrialPeriod()
    {
        return isset($this->aPlanSettings['trial_period']) ? abs($this->aPlanSettings['trial_period']) : 0;
    }
    
    public function getRegularPeriod()
    {
        return isset($this->aPlanSettings['regular_period']) ? abs($this->aPlanSettings['regular_period']) : 0;
    }
    
    public function getPackageType()
    {
        return get_post_type($this->planID);
    }
    
    private function verifyCoupon()
    {
        if (!empty($this->couponCode)) {
            $instCoupon = new Coupon();
            $instCoupon->getCouponID($this->couponCode);
            $instCoupon->getCouponSlug();
            $instCoupon->getCouponInfo();
            if (!$instCoupon->isCouponExpired() && $instCoupon->isPostTypeSupported(get_post_type($this->planID))) {
                $this->aCouponInfo           = $instCoupon->aSettings;
                $this->aCouponInfo['amount'] = $this->roundPrice($this->aCouponInfo['amount']);
                
                if ($this->aCouponInfo['type'] == 'percentage') {
                    $this->discountPrice = $this->roundPrice($this->getTotal() * $this->aCouponInfo['amount'] / 100);
                } else {
                    $this->discountPrice = $this->aCouponInfo['amount'];
                }
            }
        }
    }
    
    private function setupWooCommercePlan()
    {
    
    }
    
    public function setupPlan()
    {
        // Set up plan
        if (!isset($this->aInfo['planID'])) {
            Message::error(esc_html__('The plan is required', 'wiloke-listing-tools'));
            
            return false;
        }
        
        if (isset($this->aInfo['productID']) && !empty($this->aInfo['productID'])) {
            $this->productID     = abs($this->aInfo['productID']);
            $this->isWooCommerce = true;
            $this->orderID       = $this->aInfo['orderID'];
            $this->oProduct      = wc_get_product($this->productID);
        }
        
        $this->planID        = trim($this->aInfo['planID']);
        $this->aPlanSettings = GetSettings::getPlanSettings($this->planID);
        
        // Coupon
        $this->setCouponCode();
        $this->verifyCoupon();
        
        // Set UserID
        $this->setUserID($this->aInfo['userID']);
        $this->setupCategorySession();
    }
    
    public function getPaymentData($aAdditionalData = [])
    {
        return array_merge(
            [
                'planID'   => $this->planID,
                'userID'   => $this->getUserID(),
                'currency' => $this->getCurrency(),
                'total'    => $this->getTotal(),
                'discount' => $this->getDiscount(),
                'category' => $this->category,
                'gateway'  => $this->gateway,
                'planName' => $this->getPlanName()
            ],
            $aAdditionalData
        );
    }
}

<?php

namespace WilokeListingTools\Framework\Payment\Receipt;

use WilokeListingTools\Controllers\Retrieve\NormalRetrieve;
use WilokeListingTools\Controllers\RetrieveController;

final class ReceiptStaticFactory
{
    /**
     * @param string $category addlisting|promotion
     * @param        $aInfo
     *
     * @return AddListingReceiptStructure
     */
    public static function get($category, $aInfo)
    {
        $oRetrieve = new RetrieveController(new NormalRetrieve());
        
        switch ($category) {
            case 'addlisting':
                $oReceipt = new AddListingReceiptStructure($aInfo);
                break;
            case 'promotion':
                $oReceipt = new WilokePromotionReceipt($aInfo);
                break;
        }
        
        if (isset($oReceipt)) {
            return $oReceipt;
        }
        
        return $oRetrieve->error([
            'msg' => 'Unknown gateway receipt'
        ]);
    }
}

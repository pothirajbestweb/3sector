<?php
/*
 * Template Name: User Plan Info
 */

get_header();
global $post;
use WilokeListingTools\Framework\Helpers\WooCommerce;
use \WilokeListingTools\Framework\Helpers\FileSystem;
use WilokeListingTools\Models\PaymentMetaModel;
use WilokeListingTools\Models\PaymentModel;

//var_dump(PaymentMetaModel::getNextBillingDateGMT(160));die;

function updateUserPlan($aInfo)
{
    if ($aInfo['status'] !== 'succeeded' && $aInfo['status'] !== 'active') {
        return false;
    }
    
    if (!isset($aInfo['paymentID']) || empty($aInfo['paymentID'])) {
        FileSystem::logError('The payment id is required', __CLASS__, __METHOD__);
        
        return false;
    }
    
    $aPaymentMetaInfo = PaymentMetaModel::getPaymentInfo($aInfo['paymentID']);
   
    if (!isset($aPaymentMetaInfo['category']) || !in_array($aPaymentMetaInfo['category'], [
            'addlisting',
            'paidClaim'
        ])
    ) {
        return false;
    }
    
    if (!isset($aPaymentMetaInfo['postID']) || empty($aPaymentMetaInfo['postID'])) {
        FileSystem::logError('The post ID id is required', __CLASS__, __METHOD__);
        
        return false;
    }
    
    $userID       = PaymentModel::getField('userID', $aInfo['paymentID']);
    $aPaymentInfo = PaymentModel::getPaymentInfo($aInfo['paymentID']);

    if (empty($aPaymentInfo)) {
        FileSystem::logError(sprintf(
            'The payment %s does not exist', __CLASS__, __METHOD__,
            $aInfo['paymentID']
        ));
        
        return false;
    }
    
    $instUserModel = new \WilokeListingTools\Models\UserModel();
    $aPostIDs      = explode(',', $aPaymentMetaInfo['postID']);
    
    $postType = get_post_type($aPostIDs[0]);
    $instUserModel->setUserID($userID)
                  ->setBillingType($aPaymentInfo['billingType'])
                  ->setGateway($aPaymentInfo['gateway'])
                  ->setPaymentID($aPaymentInfo['ID'])
                  ->setPlanID($aPaymentInfo['planID'])
                  ->setPostType($postType)
    ;
    
    if (!\WilokeListingTools\Framework\Helpers\GetWilokeSubmission::isNonRecurringPayment($aPaymentInfo['billingType'])) {
        $instUserModel->setNextBillingDateGMT($aInfo['nextBillingDateGMT']);
    }
    
//    $isPassed = $this->middleware(['validateBeforeSetUserPlan'], [
//        'instUserModel' => $instUserModel,
//        'billingType'   => $aInfo['billingType'],
//        'isBoolean'      => true
//    ]);

//    if (!$isPassed) {
//        FileSystem::logError(
//            'We could not update user plan because the info wont passed validation. '.json_encode($aInfo),
//            __CLASS__,
//            __METHOD__
//        );
//
//        return false;
//    }
    
    $status = $instUserModel->setUserPlan();
    
    if (!$status) {
        FileSystem::logError('We could not set User Plan', __CLASS__, __METHOD__);
        
        return false;
    }
    FileSystem::logSuccess('Update User Plan - UserID: '.$userID.' Payment Info: '.json_encode($aPaymentInfo));
}

//updateUserPlan(
//    [
//        'planID'             => 9326,
//        'userID'             => 39,
//        'currency'           => 'USD',
//        'total'              => 5,
//        'discount'           => 0,
//        'category'           => 'addlisting',
//        'gateway'            => 'woocommerce',
//        'planRelationshipID' => false,
//        'claimID'            => false,
//        'billingType'        => 'RecurringPayment',
//        'nextBillingDateGMT' => null,
//        'status'             => 'active',
//        'paymentID'          => 152
//    ]
//);

//add_action('init', function() {
//    var_dump(\WilokeListingTools\Framework\Helpers\WooCommerce::isSubscriptionProduct(9325));die;
//});

//$billingType  = WooCommerce::getBillingType(9358);
//$oSubscription = new WC_Subscription(9386);
//$nextBillingDateGMT = $oSubscription->get_date('next_payment');
//var_dump(\WilokeListingTools\Framework\Helpers\Time::timestampUTCNow($nextBillingDateGMT));die;
//$orderID           = empty($orderID) ? $this->orderID : $orderID;
//$orderID     = 9384;
//$oOrder      = new \WC_Order($orderID);
//$aPaymentIDs = \WilokeListingTools\Models\PaymentModel::getPaymentIDsByWooOrderID($orderID, false);
//var_dump($aPaymentIDs);die;

//add_action('init', function () {
if (isset($_REQUEST['action']) && $_REQUEST['action'] == 'clearall') {
    \WilokeListingTools\Framework\Helpers\SetSettings::deleteUserMeta(get_current_user_id(),
        wilokeListingToolsRepository()->get('user:userPlans'));
}

if (isset($_REQUEST['action']) && $_REQUEST['action'] == 'clearall_payment') {
    global $wpdb;
    
    $paymentTbl = $wpdb->prefix.\WilokeListingTools\AlterTable\AlterTablePaymentHistory::$tblName;
    $paymentIDs = $wpdb->get_results(
        $wpdb->prepare(
            "SELECT * FROM $paymentTbl WHERE userID=%d",
            get_current_user_id()
        )
    );
    
    $x = $wpdb->delete(
        $paymentTbl,
        [
            'userID' => get_current_user_id()
        ],
        [
            '%d'
        ]
    );
    
    //    $invoiceTblMeta = $wpdb->prefix.\WilokeListingTools\AlterTable\AlterTableInvoiceMeta::$tblName;
    $invoiceTbl = $wpdb->prefix.\WilokeListingTools\AlterTable\AlterTableInvoices::$tblName;
    
    foreach ($paymentIDs as $oPayment) {
        $wpdb->delete(
            $invoiceTbl,
            [
                'paymentID' => $oPayment->ID
            ],
            [
                '%d'
            ]
        );
    }
    
}

if (isset($_REQUEST['action']) && $_REQUEST['action'] == 'clearall_plan_relationship') {
    global $wpdb;
    
    $paymentTbl = $wpdb->prefix.\WilokeListingTools\AlterTable\AlterTablePlanRelationships::$tblName;
    
    $wpdb->delete(
        $paymentTbl,
        [
            'userID' => get_current_user_id()
        ],
        [
            '%d'
        ]
    );
}
//});

if (have_posts()) {
    while (have_posts()) {
        the_post();
        $aUserPlans = \WilokeListingTools\Framework\Helpers\GetSettings::getUserPlans(get_current_user_id());
        
        if (!empty($aUserPlans)) {
            echo '<h1>User Plans</h1>';
            echo '<pre>';
            var_export($aUserPlans);
            echo '</pre>';
            echo '<hr>';
            
            echo '<h1>Next Billing Date GMT</h1>';
            foreach ($aUserPlans as $planKey => $aPlans) {
                foreach ($aPlans as $aPlan) {
                    $aPlan['subscriptionID'] = \WilokeListingTools\Models\PaymentMetaModel::getSubscriptionID
                    ($aPlan['paymentID']);
                    
                    if (isset($aPlan['nextBillingDateGMT'])) {
                        $aPlan['nextBillingDateGMT'] = date(get_option('date_format'), $aPlan['nextBillingDateGMT']);
                    }
                    
                    echo '<pre>';
                    var_export($aPlan);
                    echo '</pre>';
                    
                }
                echo '<hr>';
            }
        } else {
            echo 'We found no plan';
        }
        
    }
    ?>
    <a style="color: red" href="<?php echo add_query_arg(['action' => 'clearall'], get_permalink($post->ID)); ?>">
        ClearAll Plan
    </a>
    <br>
    <a style="color: red" href="<?php echo add_query_arg(['action' => 'clearall_payment'], get_permalink($post->ID));
    ?>">
        Clear Payment IDs
    </a>

    <a style="color: red" href="<?php echo add_query_arg(['action' => 'clearall_plan_relationship'], get_permalink
    ($post->ID));
    ?>">
        Clear All Plan Relationship
    </a>
    <?php
}
get_footer();
